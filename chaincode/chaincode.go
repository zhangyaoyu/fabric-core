package chaincode

import (
	"fmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	contextAPI "github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	fabAPI "github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	contextImpl "github.com/hyperledger/fabric-sdk-go/pkg/context"
	lcpackager "github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/lifecycle"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/policydsl"
	"github.com/pkg/errors"
	"gitlab.com/zhangyaoyu/fabric-core.git/pkg/client"
	"gitlab.com/zhangyaoyu/fabric-core.git/pkg/context"
)

type Lifecycle interface {
	DeployChainCode(deployChainCodeReq *client.DeployChainCodeReq) (*fabAPI.TransactionID, error)
	UpgradeChainCode()
	ChainCodeQuery(ChainCodeReq *client.ChainCodeReq) (*client.ChainCodeCallResp, error)
	ChainCodeInvoke(ChainCodeReq *client.ChainCodeReq) (*client.ChainCodeCallResp, error)
	installChainCode(installChainCodeReq *client.InstallChainCodeReq, orgContext *context.OrgContext) (string, error)
	approveChainCode(approveChainCodeReq *client.ApproveChainCodeReq, orgContext *context.OrgContext) error
	commitChainCode(commitChainCodeReq *client.CommitChainCodeReq, orgContext *context.OrgContext) (*fabAPI.TransactionID, error)
}

type lifecycle struct {
	client client.Client
}

func NewLifecycle(client client.Client) Lifecycle {
	return &lifecycle{client: client}
}

func (l *lifecycle) UpgradeChainCode() {
	//TODO implement me
}

// DeployChainCode deploy chaincode
func (l *lifecycle) DeployChainCode(deployChainCodeReq *client.DeployChainCodeReq) (*fabAPI.TransactionID, error) {
	// install chaincode
	sdk := l.client.GetSDK()
	var orgContext = &context.OrgContext{
		OrgAdminClientContext: sdk.Context(fabsdk.WithOrg(deployChainCodeReq.Org), fabsdk.WithUser(deployChainCodeReq.User)),
	}
	orgResMgmt, err := resmgmt.New(orgContext.OrgAdminClientContext)
	if err != nil {
		return nil, err
	}
	orgContext.OrgResMgmt = orgResMgmt
	sdk.Context(fabsdk.WithUser(deployChainCodeReq.User), fabsdk.WithOrg(deployChainCodeReq.Org))
	installChainCodeReq := &client.InstallChainCodeReq{
		ChaincodeLabel: deployChainCodeReq.ChaincodeName + "_" + deployChainCodeReq.ChaincodeVersion,
		ChannelID:      deployChainCodeReq.ChannelID,
		ChainCodeBytes: deployChainCodeReq.ChainCodeBytes,
	}
	packageID, err := l.installChainCode(installChainCodeReq, orgContext)
	if err != nil {
		return nil, err
	}

	// approve chaincode
	approveChainCodeReq := &client.ApproveChainCodeReq{
		ChaincodeVersion: deployChainCodeReq.ChaincodeVersion,
		ChaincodeName:    deployChainCodeReq.ChaincodeName,
		ChannelID:        deployChainCodeReq.ChannelID,
		PackageID:        packageID,
		ChainCodePolicy:  deployChainCodeReq.ChainCodePolicy,
	}
	err = l.approveChainCode(approveChainCodeReq, orgContext)
	if err != nil {
		return nil, err
	}

	//commit chaincode
	commitChainCodeReq := &client.CommitChainCodeReq{
		ChannelID:        deployChainCodeReq.ChannelID,
		ChaincodeName:    deployChainCodeReq.ChaincodeName,
		ChaincodeVersion: deployChainCodeReq.ChaincodeVersion,
		ChainCodePolicy:  deployChainCodeReq.ChainCodePolicy,
	}
	txnID, err := l.commitChainCode(commitChainCodeReq, orgContext)
	if err != nil {
		return nil, err
	}
	return txnID, nil
}

func (l *lifecycle) ChainCodeQuery(ChainCodeReq *client.ChainCodeReq) (*client.ChainCodeCallResp, error) {
	//prepare channel client context using client context
	sdk := l.client.GetSDK()
	clientChannelContext := sdk.ChannelContext(ChainCodeReq.ChannelID, fabsdk.WithUser(ChainCodeReq.User), fabsdk.WithOrg(ChainCodeReq.Org))
	// Channel client is used to query and execute transactions (Org1 is default org)
	channelClient, err := channel.New(clientChannelContext)
	if err != nil {
		fmt.Printf("Failed to create new channel client: %s", err)
		return nil, err
	}
	response, err := channelClient.Query(channel.Request{ChaincodeID: ChainCodeReq.ChaincodeName, Fcn: ChainCodeReq.Function, Args: ChainCodeReq.Args})
	if err != nil {
		fmt.Printf("Failed to query,error = %s", err)
		return nil, err
	}

	var chainCodeCallResp = &client.ChainCodeCallResp{
		TransactionID: response.TransactionID,
		Payload:       string(response.Payload),
	}
	return chainCodeCallResp, nil
}

func (l *lifecycle) ChainCodeInvoke(ChainCodeReq *client.ChainCodeReq) (*client.ChainCodeCallResp, error) {
	//prepare channel client context using client context
	sdk := l.client.GetSDK()
	clientChannelContext := sdk.ChannelContext(ChainCodeReq.ChannelID, fabsdk.WithUser(ChainCodeReq.User), fabsdk.WithOrg(ChainCodeReq.Org))
	// Channel client is used to query and execute transactions (Org1 is default org)
	channelClient, err := channel.New(clientChannelContext)
	if err != nil {
		fmt.Printf("Failed to create new channel client: %s", err)
		return nil, err
	}
	response, err := channelClient.Execute(channel.Request{ChaincodeID: ChainCodeReq.ChaincodeName, Fcn: ChainCodeReq.Function, Args: ChainCodeReq.Args})
	if err != nil {
		fmt.Printf("Failed to invoke chaincode:%s\n", err)
		return nil, err
	}
	if response.ChaincodeStatus == 0 {
		fmt.Printf("Expected ChaincodeStatus")
		return nil, errors.New("Expected ChaincodeStatus")
	}
	if response.Responses[0].ChaincodeStatus != response.ChaincodeStatus {
		fmt.Printf("Expected the chaincode status returned by successful Peer Endorsement to be same as Chaincode status for client response")
		return nil, errors.New("Expected the chaincode status returned by successful Peer Endorsement to be same as Chaincode status for client response")
	}
	block, err := l.client.GetBlockByTxID(&client.UserIdentity{User: ChainCodeReq.User, Org: ChainCodeReq.Org}, ChainCodeReq.ChannelID, response.TransactionID)
	if err != nil {
		fmt.Printf("Failed to get block by txID:%s\n", err)
	}
	var chainCodeResp = &client.ChainCodeCallResp{
		TransactionID: response.TransactionID,
		BlockHeight:   block.Header.Number,
		Payload:       string(response.Payload),
	}
	return chainCodeResp, nil
}

func (l *lifecycle) installChainCode(installChainCodeReq *client.InstallChainCodeReq, orgContext *context.OrgContext) (string, error) {
	installCCReq := resmgmt.LifecycleInstallCCRequest{
		Label:   installChainCodeReq.ChaincodeLabel,
		Package: installChainCodeReq.ChainCodeBytes,
	}

	packageID := lcpackager.ComputePackageID(installCCReq.Label, installCCReq.Package)
	orgPeers, err := discoverLocalPeers(orgContext.OrgAdminClientContext)
	if err != nil {
		fmt.Printf("DiscoverLocalPeers error,error = %s\n", err)
	}
	if !checkInstalled(packageID, orgPeers[0], orgContext.OrgResMgmt) {
		resp, err := orgContext.OrgResMgmt.LifecycleInstallCC(installCCReq, resmgmt.WithTargets(orgPeers...))
		if err != nil {
			fmt.Printf("LifecycleInstallCC err,error =%s\n", err)
			return "", err
		}
		if resp[0].PackageID != packageID {
			fmt.Println("Expected the package ID returned by LifecycleInstallCC to be same as the one passed in the request")
			return "", errors.New("Expected the package ID returned by LifecycleInstallCC to be same as the one passed in the request")
		}
	}
	return packageID, nil
}

func (l *lifecycle) approveChainCode(approveChainCodeReq *client.ApproveChainCodeReq, orgContext *context.OrgContext) error {
	//ccPolicy := policydsl.SignedByNOutOfGivenRole(1, mb.MSPRole_MEMBER, []string{"Org1MSP"})
	ccPolicy := policydsl.SignedByAnyMember(approveChainCodeReq.ChainCodePolicy)
	approveCCReq := resmgmt.LifecycleApproveCCRequest{
		Name:              approveChainCodeReq.ChaincodeName,
		Version:           approveChainCodeReq.ChaincodeVersion,
		PackageID:         approveChainCodeReq.PackageID,
		Sequence:          1,
		EndorsementPlugin: "escc",
		ValidationPlugin:  "vscc",
		SignaturePolicy:   ccPolicy,
		InitRequired:      false,
	}
	fmt.Printf("req = %+v\n", approveCCReq)
	orgPeers, err := discoverLocalPeers(orgContext.OrgAdminClientContext)
	if err != nil {
		fmt.Printf("DiscoverLocalPeers error,error = %s\n", err)
	}
	txnID, err := orgContext.OrgResMgmt.LifecycleApproveCC(approveChainCodeReq.ChannelID, approveCCReq, resmgmt.WithTargets(orgPeers...))
	if err != nil {
		fmt.Printf("orgResMgmt.LifecycleApproveCC error ,error = %s\n", err)
		return err
	}
	fmt.Printf("orgResMgmt.LifecycleApproveCC success, txnID=%s\n", txnID)
	return nil
}

func (l *lifecycle) commitChainCode(commitChainCodeReq *client.CommitChainCodeReq, orgContext *context.OrgContext) (*fabAPI.TransactionID, error) {
	//ccPolicy := policydsl.SignedByNOutOfGivenRole(1, mb.MSPRole_MEMBER, []string{"Org1MSP"})
	ccPolicy := policydsl.SignedByAnyMember(commitChainCodeReq.ChainCodePolicy)
	req := resmgmt.LifecycleCommitCCRequest{
		Name:              commitChainCodeReq.ChaincodeName,
		Version:           commitChainCodeReq.ChaincodeVersion,
		Sequence:          1,
		EndorsementPlugin: "escc",
		ValidationPlugin:  "vscc",
		SignaturePolicy:   ccPolicy,
		InitRequired:      false,
	}
	fmt.Printf("req = %+v\n", req)
	orgPeers, err := discoverLocalPeers(orgContext.OrgAdminClientContext)
	if err != nil {
		fmt.Printf("DiscoverLocalPeers error,error = %s\n", err)
	}
	txnID, err := orgContext.OrgResMgmt.LifecycleCommitCC(commitChainCodeReq.ChannelID, req, resmgmt.WithTargets(orgPeers...))
	if err != nil {
		fmt.Printf("提交链码失败,error = %s\n", err)
		return nil, err
	}
	return &txnID, nil
}

func discoverLocalPeers(ctxProvider contextAPI.ClientProvider) ([]fabAPI.Peer, error) {
	ctx, err := contextImpl.NewLocal(ctxProvider)
	if err != nil {
		return nil, errors.Wrap(err, "error creating local context")
	}

	discoveredPeers, err := retry.NewInvoker(retry.New(retry.TestRetryOpts)).Invoke(
		func() (interface{}, error) {
			peers, serviceErr := ctx.LocalDiscoveryService().GetPeers()
			if serviceErr != nil {
				return nil, errors.Wrapf(serviceErr, "error getting peers for MSP [%s]", ctx.Identifier().MSPID)
			}
			return peers, nil
		},
	)
	if err != nil {
		return nil, err
	}

	return discoveredPeers.([]fabAPI.Peer), nil
}
func checkInstalled(packageID string, peer fab.Peer, client *resmgmt.Client) bool {
	flag := false
	resp1, err := client.LifecycleQueryInstalledCC(resmgmt.WithTargets(peer))
	if err != nil {
		fmt.Printf("LifecycleQueryInstalledCC error,error = %s\n", err)
	}
	for _, t := range resp1 {
		if t.PackageID == packageID {
			flag = true
		}
	}
	return flag
}
