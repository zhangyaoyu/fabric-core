package util

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"os"
	"strings"
)

func GetTarBytes(file *os.File) ([]byte, error) {
	// 创建gzip.Reader
	gr, err := gzip.NewReader(file)
	if err != nil {
		return nil, err
	}
	defer func(gr *gzip.Reader) {
	}(gr)

	// 创建tar.Reader
	tr := tar.NewReader(gr)
	codeBytes := bytes.NewBuffer(nil)
	metaBytes := bytes.NewBuffer(nil)

	// 遍历tar文件中的每个文件
	for {
		header, err := tr.Next()
		if err == io.EOF {
			// 已到达文件末尾
			break
		}
		if err != nil {

		}
		fmt.Printf("文件名：%s, 文件大小：%d\n", header.Name, header.Size)
		if header.Name == "metadata.json" {
			all, err := io.ReadAll(tr)
			if err != nil {
				return nil, err
			}
			metaBytes.Write(all)
		}
		if header.Name == "code.tar.gz" {
			all, err := io.ReadAll(tr)
			if err != nil {
				return nil, err
			}
			codeBytes.Write(all)
		}
	}
	payload := bytes.NewBuffer(nil)
	gw := gzip.NewWriter(payload)
	tw := tar.NewWriter(gw)
	err = writePackage(tw, "metadata.json", metaBytes.Bytes())
	if err != nil {
		return nil, err
	}
	err = writePackage(tw, "code.tar.gz", codeBytes.Bytes())
	if err != nil {
		return nil, err
	}
	err = tw.Close()
	if err == nil {
		err = gw.Close()
	}
	return payload.Bytes(), nil
}

func writePackage(tw *tar.Writer, name string, payload []byte) error {
	err := tw.WriteHeader(
		&tar.Header{
			Name: name,
			Size: int64(len(payload)),
			Mode: 0100644,
		},
	)
	if err != nil {
		return err
	}
	_, err = tw.Write(payload)
	return err
}
func GetKeys(m map[string]interface{}) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		if strings.HasPrefix(k, "org") {
			keys = append(keys, k)
		}
	}
	return keys
}
func LoadCertificate(certBytes []byte) (*x509.Certificate, error) {
	block, _ := pem.Decode(certBytes)
	if block == nil {
		fmt.Printf("ERROR: block of decoded private key is nil\n")
		return nil, errors.New("block of decoded private key is nil")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		fmt.Printf("ERROR: failed get ECDSA private key, error: %v\n", err)
		return nil, err
	}

	return cert, nil
}

func ValidUserIdentity(hash string, pk []byte) bool {
	sum := sha256.Sum256(pk)
	return hex.EncodeToString(sum[:]) == hash
}
