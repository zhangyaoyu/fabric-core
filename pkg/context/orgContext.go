package context

import (
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	contextAPI "github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
)

type OrgContext struct {
	OrgAdminClientContext contextAPI.ClientProvider
	OrgResMgmt            *resmgmt.Client
}
