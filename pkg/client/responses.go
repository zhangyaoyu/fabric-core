package client

import "github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"

type ChainCodeCallResp struct {
	TransactionID fab.TransactionID `json:"transactionId"` //交易ID
	BlockHeight   uint64            `json:"blockHeight"`   //区块高度
	Payload       string            `json:"payload"`       //数据
}
