package client

type DeployChainCodeReq struct {
	ChaincodeVersion string                `json:"chaincodeVersion"` // 链码版本
	ChaincodeName    string                `json:"chaincodeName"`    // 链码名称
	ChainCodePolicy  []string              `json:"chaincodePolicy"`  // 链码策略
	ChainCodeBytes   []byte                `json:"chaincodeBytes"`   // 链码文件
	ChannelID        string                `json:"channelId"`        // 所属通道ID
	*UserIdentity    `json:"userIdentity"` // 用户身份
}

type InstallChainCodeReq struct {
	ChaincodeLabel string `json:"chaincodeLabel"` // 链码标签
	ChannelID      string `json:"channelId"`      // 所属通道ID
	ChainCodeBytes []byte `json:"chaincodeBytes"` // 链码文件
}

type ApproveChainCodeReq struct {
	ChaincodeVersion string   `json:"chaincodeVersion"` // 链码版本
	ChaincodeName    string   `json:"chaincodeName"`    // 链码名称
	ChannelID        string   `json:"channelId"`        // 所属通道ID
	PackageID        string   `json:"packageId"`        // 链码包ID
	ChainCodePolicy  []string `json:"chaincodePolicy"`  // 链码策略
}

type CommitChainCodeReq struct {
	ChannelID        string   `json:"channelId"`        // 所属通道ID
	ChaincodeName    string   `json:"chaincodeName"`    // 链码名称
	ChaincodeVersion string   `json:"chaincodeVersion"` // 链码版本
	ChainCodePolicy  []string `json:"chaincodePolicy"`  // 链码策略
}

type ChainCodeReq struct {
	ChaincodeName string                `json:"chaincodeName"` // 链码名称
	Args          [][]byte              `json:"args"`          // 传入参数
	Function      string                `json:"function"`      // 函数名
	ChannelID     string                `json:"channelId"`     // 所属通道ID
	*UserIdentity `json:"userIdentity"` // 用户身份
}

type UserIdentity struct {
	Org    string `json:"org" form:"org" binding:"required"`       // 组织名
	User   string `json:"user" form:"user" binding:"required"`     // 用户名
	PKHash string `json:"pkHash" form:"pkHash" binding:"required"` // 用户私钥哈希
}
