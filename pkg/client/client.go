package client

import (
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	fabAPI "github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

type Client interface {
	GetBlockByNumber(*UserIdentity, string, uint64) (*common.Block, error)
	GetBlockByTxID(*UserIdentity, string, fabAPI.TransactionID) (*common.Block, error)
	GetSDK() *fabsdk.FabricSDK
}

func NewClient(configPath string) Client {
	sdk, _ := fabsdk.New(config.FromFile(configPath))
	return &client{sdk: sdk}
}

type client struct {
	sdk *fabsdk.FabricSDK
}

func (client *client) GetBlockByNumber(identity *UserIdentity, channelID string, number uint64) (*common.Block, error) {
	userChannelContext := client.sdk.ChannelContext(channelID, fabsdk.WithOrg(identity.Org), fabsdk.WithUser(identity.User))
	ledgerClient, _ := ledger.New(userChannelContext)
	block, err := ledgerClient.QueryBlock(number)
	if err != nil {
		return nil, err
	}
	return block, nil
}

func (client *client) GetBlockByTxID(identity *UserIdentity, channelID string, txID fabAPI.TransactionID) (*common.Block, error) {
	userChannelContext := client.sdk.ChannelContext(channelID, fabsdk.WithOrg(identity.Org), fabsdk.WithUser(identity.User))
	ledgerClient, _ := ledger.New(userChannelContext)
	block, err := ledgerClient.QueryBlockByTxID(txID)
	if err != nil {
		return nil, err
	}
	return block, nil
}

func (client *client) GetSDK() *fabsdk.FabricSDK {
	return client.sdk
}
